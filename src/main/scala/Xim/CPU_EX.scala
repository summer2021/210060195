package Xim

import chisel3._
import chisel3.util.{Fill, Cat}

object excode_const extends ExceptionConstants

class CPU_EX(val rv_width: Int = 64) extends Module {
    val io = IO(new Bundle {
        val data_addr = Output(UInt(rv_width.W))
        val data_write_mem = Output(UInt(1.W))
        val data_read_mem = Output(UInt(1.W))
        val data_write_mmio = Output(UInt(1.W))
        val data_read_mmio = Output(UInt(1.W))
        val data_size = Output(UInt(2.W))
        val is_mmio = Output(UInt(1.W))
        
        val data_write_data = Output(UInt(rv_width.W))
        
        val data_req_ack = Input(UInt(1.W))
        
        val data_read_data = Input(UInt(rv_width.W))
        val data_read_valid = Input(UInt(1.W))
        val data_data_ack = Output(UInt(1.W))


        // to ds
        val es_allowin = Output(UInt(1.W))
        // val inst_reload = Output(UInt(1.W))
        
        val ds_to_es_valid = Input(UInt(1.W))
        val ds_pc = Input(UInt(rv_width.W))
        val ds_inst = Input(UInt(32.W))
        val ds_ex = Input(UInt(1.W))
        val ds_excode = Input(UInt(5.W))
        val inst_reload = Input(Bool())
        val ds_to_es_control_signals = Input(new DsToEsBundle)


        // to ws
        val es_to_ws_wen = Output(Bool())
        val es_to_ws_waddr = Output(UInt(5.W))
        val es_to_ws_wdata = Output(UInt(rv_width.W))

        val ws_allow_in = Input(Bool())
        val es_to_ws_valid = Output(Bool())
        val es_to_ws_pc = Output(UInt(rv_width.W))
        val es_to_ws_ex = Output(Bool())
        val es_to_ws_excode = Output(UInt(5.W))

        
        val ex_valid = Output(UInt(1.W))
        val ex_target = Output(UInt(rv_width.W))
        
        val es_instr = Output(UInt(32.W))
        val es_pc = Output(UInt(rv_width.W))

        // for CSR and Priviledge Signal
        val es_inst_valid = Output(UInt(1.W))
        val es_inst_mret = Output(UInt(1.W))
        val es_inst_sret = Output(UInt(1.W))
        val mepc = Input(UInt(rv_width.W))
        val sepc = Input(UInt(rv_width.W))
        val new_instr = Output(UInt(1.W))
        val csr_excode = Output(UInt(rv_width.W))
        val es_csr_inst = Output(UInt(1.W))
        val csr_inst_reserved = Output(UInt(1.W))
        val csr_number = Output(UInt(12.W))
        val csr_read_data = Input(UInt(rv_width.W))
        val csr_write_data = Output(UInt(rv_width.W))
        val trap_entry = Input(UInt(rv_width.W))
        val csr_timer_int = Input(UInt(1.W))
        val mstatus_tsr = Input(UInt(1.W))
        val priv_level = Input(UInt(2.W))
        val illegal_csr = Input(Bool())
        
        
        // forward to ID
        val es_forward_valid = Output(Bool())
        val es_forward_waddr = Output(Bool())
        val es_forward_wdata = Output(UInt(rv_width.W))
    })
    
    val es_valid = RegInit(0.U(1.W))
    // es_allowin as Output
    val es_ready_go = Wire(UInt(1.W))
    val es_pc = Reg(UInt(rv_width.W))
    val es_instr = Reg(UInt(32.W))
    val es_ex = Wire(UInt(1.W))
    val es_excode = Wire(UInt(rv_width.W))
    val es_new_instr = Wire(UInt(1.W))
    val es_new_instr_r = RegInit(0.U(1.W))
    val es_next_branch = RegInit(0.U(1.W))
    
    io.es_instr := es_instr
    io.es_pc := es_pc
    
    val CSR_read_data = Wire(UInt(rv_width.W))
    io.ex_target := io.trap_entry
    val CSR_mstatus_tsr = Wire(UInt(1.W))
    val es_csr = Wire(UInt(1.W))
    val timer_int = Wire(UInt(1.W))
    val timer_int_r = RegInit(0.U(1.W))
    val CSR_mepc = Wire(UInt(rv_width.W))
    val CSR_sepc = Wire(UInt(rv_width.W))
    
    
    
    // decode related
    val es_load = Wire(UInt(1.W))
    val es_store = Wire(UInt(1.W))
    val es_branch = Wire(UInt(1.W))
    //val es_load_r = RegInit(0.U(1.W))
    //val es_store_r = RegInit(1.U(1.W))
    val es_data_handshake = Wire(UInt(1.W))
    val es_data_handshake_r = RegInit(0.U(1.W))
    val es_addr_handshake = Wire(UInt(1.W))
    val es_write_r = RegInit(0.B)
    val es_read_r = RegInit(0.B)
    // we do not need a data_handshake_r here because load and store will always end right after the handshake




    // branch and jump is finished in a cycle
    
    
    
    when((es_load === 1.U || es_store === 1.U) && es_ex === 0.U) {
        es_ready_go := es_data_handshake | es_data_handshake_r
    }.otherwise {
        es_ready_go := 1.U
    }
    
    when (es_data_handshake === 1.U) {
        es_data_handshake_r := 1.U
    } .elsewhen (es_new_instr === 1.U) {
        es_data_handshake_r := 0.U
    }
    
    io.es_allowin := (!(es_valid === 1.U)) || (es_ready_go === 1.U && io.ws_allow_in)
    
    es_new_instr := io.es_allowin === 1.U && io.ds_to_es_valid === 1.U

    io.es_to_ws_valid := es_valid
    io.es_to_ws_ex := es_ex
    io.es_to_ws_excode := es_excode
    io.es_to_ws_pc := es_pc

    val ds_fwd_signals_r = Reg(new DsToEsBundle)
    
    /* when(es_new_instr === 1.U && io.inst_reload) {
        es_instr := 0x00000033.U // provide a nop instruction (add zero, zero, zero)
        ds_fwd_signals_r := io.ds_to_es_control_signals
    }.else */
    when(es_new_instr === 1.U) {
        es_instr := io.ds_inst
        ds_fwd_signals_r := io.ds_to_es_control_signals
    }
    when (es_ready_go === 1.U && io.ws_allow_in && !es_new_instr) {
        es_new_instr_r := 0.U
    } .elsewhen (es_new_instr === 1.U) {
        es_new_instr_r := 1.U
    }

    
    /*
    when (inst_reload === 1.U) {
        es_valid := 0.U
    } .elsewhen(es_new_instr_r === 1.U) {
        es_valid := 1.U
        // TODO: check exception conditions here
    }*/
    when(es_new_instr === 1.U) {
        es_valid := !io.inst_reload
    } .elsewhen (es_ready_go === 1.U && io.ws_allow_in) {
        es_valid := 0.U
    }
    
    when(es_new_instr === 1.U) {
        es_pc := io.ds_pc
    }

    es_load := ds_fwd_signals_r.inst_load
    es_store := ds_fwd_signals_r.inst_store
    es_branch := ds_fwd_signals_r.inst_branch
    io.csr_number := ds_fwd_signals_r.csr_number
    
    // Execute stage
    val es_alu = Module(new ALU)
    val alu_result = Wire(UInt(rv_width.W))
    val alu_overflow = Wire(UInt(1.W))
    // val alu_op = Wire(UInt(12.W))
    
    
    when (ds_fwd_signals_r.src1_is_rs1 === 1.U && ds_fwd_signals_r.inst_csrrc === 0.U) {
        es_alu.io.alu_src1 := ds_fwd_signals_r.reg_rdata_1
    } .elsewhen (ds_fwd_signals_r.src1_is_rs1 === 1.U && ds_fwd_signals_r.inst_csrrc === 1.U) {
        es_alu.io.alu_src1 := ~ds_fwd_signals_r.reg_rdata_1
    } .elsewhen(ds_fwd_signals_r.src1_is_imm === 1.U) {
        es_alu.io.alu_src1 := ds_fwd_signals_r.inst_imm
    } .otherwise {
        es_alu.io.alu_src1 := 0.U;
    }
    
    
    when(ds_fwd_signals_r.src2_is_imm === 1.U) {
        es_alu.io.alu_src2 := ds_fwd_signals_r.inst_imm
    }.elsewhen(ds_fwd_signals_r.src2_is_pc === 1.U) {
        es_alu.io.alu_src2 := es_pc
    }.elsewhen(ds_fwd_signals_r.src2_is_rs2 === 1.U && ds_fwd_signals_r.truncatShift === 0.U) {
        es_alu.io.alu_src2 := ds_fwd_signals_r.reg_rdata_2
    }.elsewhen(ds_fwd_signals_r.src2_is_rs2 === 1.U && ds_fwd_signals_r.truncatShift === 1.U) {
        es_alu.io.alu_src2 := ds_fwd_signals_r.reg_rdata_2(4, 0)
    }.elsewhen(ds_fwd_signals_r.src2_is_rs2_self === 1.U) {
        es_alu.io.alu_src2 := Cat(es_instr(25), ds_fwd_signals_r.rs2)
    } .elsewhen (ds_fwd_signals_r.src2_is_zero === 1.U) {
        es_alu.io.alu_src2 := 0.U
    } .elsewhen (ds_fwd_signals_r.src2_is_csr === 1.U) {
        es_alu.io.alu_src2 := CSR_read_data
    } .otherwise {
        es_alu.io.alu_src2 := 0.U
    }
    
    when(ds_fwd_signals_r.alu_op_add === 1.U) {
        es_alu.io.alu_op := 1.U
    }.elsewhen(ds_fwd_signals_r.alu_op_sub === 1.U) {
        es_alu.io.alu_op := 2.U
    }.elsewhen(ds_fwd_signals_r.alu_op_slt === 1.U) {
        es_alu.io.alu_op := 4.U
    }.elsewhen(ds_fwd_signals_r.alu_op_sltu === 1.U) {
        es_alu.io.alu_op := 8.U
    }.elsewhen(ds_fwd_signals_r.alu_op_and === 1.U) {
        es_alu.io.alu_op := 16.U
    //}.elsewhen(ds_to_es_control_signals_r.alu_op_nor === 1.U) {
    //    es_alu.io.alu_op := 32.U
    }.elsewhen(ds_fwd_signals_r.alu_op_or === 1.U) {
        es_alu.io.alu_op := 64.U
    }.elsewhen(ds_fwd_signals_r.alu_op_xor === 1.U) {
        es_alu.io.alu_op := 128.U
    }.elsewhen(ds_fwd_signals_r.alu_op_sll === 1.U) {
        es_alu.io.alu_op := 256.U
    }.elsewhen(ds_fwd_signals_r.alu_op_srl === 1.U) {
        es_alu.io.alu_op := 512.U
    }.elsewhen(ds_fwd_signals_r.alu_op_sra === 1.U) {
        es_alu.io.alu_op := 1024.U
    }.elsewhen(ds_fwd_signals_r.alu_op_lui === 1.U) {
        es_alu.io.alu_op := 2048.U
    }.elsewhen(ds_fwd_signals_r.alu_op_srl_w === 1.U) {
        es_alu.io.alu_op := 4096.U
    }.elsewhen(ds_fwd_signals_r.alu_op_sra_w === 1.U) {
        es_alu.io.alu_op := 8192.U
    } .otherwise {
        es_alu.io.alu_op := 1.U
        // we do not use ALU in this case
    }
    
    alu_result := es_alu.io.alu_result
    alu_overflow := es_alu.io.alu_overflow
    
    io.data_addr := alu_result

    io.new_instr := es_new_instr
    io.csr_excode := es_excode
    io.es_csr_inst := es_csr
    io.csr_inst_reserved := ds_fwd_signals_r.inst_reserved
    
    val is_mmio = Wire(Bool())
    is_mmio := (io.data_addr < 0x80000000L.U)
    io.is_mmio := (es_load | es_store) & is_mmio
    
    io.data_write_mem := es_write_r & ~is_mmio
    io.data_read_mem := es_read_r & ~is_mmio
    io.data_write_mmio := es_write_r & is_mmio
    io.data_read_mmio := es_read_r & is_mmio
    es_data_handshake := io.data_read_valid === 1.U && io.data_data_ack === 1.U
    es_addr_handshake := (io.data_write_mmio | io.data_write_mem | io.data_read_mmio | io.data_read_mem) === 1.U && io.data_req_ack === 1.U
    val es_write_set = RegInit(0.U(1.W))
    val es_read_set = RegInit(0.U(1.W))
    val es_write_ex = Wire(UInt(1.W))
    val es_read_ex = Wire(UInt(1.W))
    
    es_write_ex := es_store === 1.U && ((ds_fwd_signals_r.inst_sw === 1.U && io.data_addr(1, 0) != 0.U)
      || (ds_fwd_signals_r.inst_sh === 1.U && io.data_addr(0) != 0.U)
      || (ds_fwd_signals_r.inst_sd === 1.U && io.data_addr(2, 0) != 0.U) )
    es_read_ex := es_load === 1.U && (((ds_fwd_signals_r.inst_lw | ds_fwd_signals_r.inst_lwu) === 1.U && io.data_addr(1, 0) != 0.U)
      || ((ds_fwd_signals_r.inst_lh | ds_fwd_signals_r.inst_lhu) === 1.U && io.data_addr(0) != 0.U)
      || (ds_fwd_signals_r.inst_ld === 1.U && io.data_addr(2, 0) != 0.U) ) // lb and lbu will not trigger this exception
    
    when (es_new_instr === 1.U) {
        es_write_set := 0.U
    } .elsewhen (es_store === 1.U) {
        es_write_set := 1.U
    }
    
    when (es_new_instr === 1.U) {
        es_read_set := 0.U
    } .elsewhen (es_load === 1.U) {
        es_read_set := 1.U
    }
    
    when (ds_fwd_signals_r.inst_sb === 1.U || ds_fwd_signals_r.inst_lb === 1.U || ds_fwd_signals_r.inst_lbu === 1.U) {
        io.data_size := 0.U
    } .elsewhen (ds_fwd_signals_r.inst_sh === 1.U || ds_fwd_signals_r.inst_lh === 1.U || ds_fwd_signals_r.inst_lhu === 1.U) {
        io.data_size := 1.U
    } .elsewhen (ds_fwd_signals_r.inst_sw === 1.U || ds_fwd_signals_r.inst_lw === 1.U || ds_fwd_signals_r.inst_lwu === 1.U) {
        io.data_size := 2.U
    } .elsewhen (ds_fwd_signals_r.inst_sd === 1.U || ds_fwd_signals_r.inst_ld === 1.U) {
        io.data_size := 3.U
    } .otherwise {
        io.data_size := 0.U
    }
    
    when (es_addr_handshake === 1.U) {
        es_read_r := 0.U
    } .elsewhen (es_load === 1.U && es_read_set === 0.U && es_ex === 0.U) {
        es_read_r := 1.U
    }
    
    when (es_addr_handshake === 1.U) {
        es_write_r := 0.U
    } .elsewhen (es_store === 1.U && es_write_set === 0.U && es_ex === 0.U) {
        es_write_r := 1.U
    }
    
    // for store instructions, always from rs2
    when (ds_fwd_signals_r.inst_sd === 1.U) {
        io.data_write_data := ds_fwd_signals_r.reg_rdata_2(63, 0).asUInt()
    } .elsewhen (ds_fwd_signals_r.inst_sw === 1.U) {
        io.data_write_data := Fill(2, ds_fwd_signals_r.reg_rdata_2(31, 0).asUInt())
    } .elsewhen (ds_fwd_signals_r.inst_sh === 1.U) {
        io.data_write_data := Fill(4, ds_fwd_signals_r.reg_rdata_2(15, 0).asUInt())
    } .elsewhen (ds_fwd_signals_r.inst_sb === 1.U) {
        io.data_write_data := Fill(8, ds_fwd_signals_r.reg_rdata_2(7, 0).asUInt())
    } .otherwise {
        io.data_write_data := 0.U
    }
    
    
    // dummy signal for now
    io.data_data_ack := 1.U
    
    // note that load instructions may only write when load data is returned
    when (es_ex === 1.U) {
        io.es_to_ws_wen := 0.U
    } .elsewhen(ds_fwd_signals_r.inst_load === 1.U && (es_data_handshake === 1.U || es_data_handshake_r === 1.U) && es_new_instr_r === 1.U) {
        // TODO: revise proper condition here
        // Problem here: the reg will continue to write even after the first written
        // The current condition indicates that if our instruction is not a load, it should go directly into the write
        // back stage, which finishes in a single cycle
        io.es_to_ws_wen := 1.U
    } .elsewhen (ds_fwd_signals_r.inst_load === 0.U && es_new_instr_r === 1.U) {
        io.es_to_ws_wen := ds_fwd_signals_r.gr_we
    } .otherwise {
        io.es_to_ws_wen := 0.U
    }
    

    CSR_read_data := io.csr_read_data
    CSR_mstatus_tsr := io.mstatus_tsr
    CSR_mepc := io.mepc
    CSR_sepc := io.sepc
    timer_int := io.csr_timer_int
    io.csr_write_data := alu_result
    io.es_inst_valid := es_valid
    io.es_inst_mret := ds_fwd_signals_r.inst_mret
    io.es_inst_sret := ds_fwd_signals_r.inst_sret

    es_csr := ds_fwd_signals_r.inst_csrrs | ds_fwd_signals_r.inst_csrrc | ds_fwd_signals_r.inst_csrrw | ds_fwd_signals_r.inst_csrrwi | ds_fwd_signals_r.inst_csrrsi | ds_fwd_signals_r.inst_csrrci
    
    
    val pc_off = Wire(UInt(rv_width.W))
    
    pc_off := es_pc + 4.U
    
    val reg_wdata_mem_r = Reg(UInt(rv_width.W))
    val consistent_reg_wdata_mem = Wire(UInt(rv_width.W))
    
    val reg_wdata_s = Wire(SInt(rv_width.W))
    val reg_wdata = Wire(UInt(rv_width.W))
    // for convenice
    
    when (es_data_handshake === 1.U) {
        reg_wdata_mem_r  := io.data_read_data
    }
    
    when (es_data_handshake === 1.U) {
        consistent_reg_wdata_mem := io.data_read_data
    } .otherwise {
        consistent_reg_wdata_mem := reg_wdata_mem_r
    }
    
    when (ds_fwd_signals_r.reg_wdata_alu === 1.U && ds_fwd_signals_r.reg_wdata_alu_lower === 0.U) {
        reg_wdata := alu_result
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_alu === 1.U && ds_fwd_signals_r.reg_wdata_alu_lower === 1.U) {
        reg_wdata := reg_wdata_s.asUInt()
        reg_wdata_s := alu_result(31, 0).asSInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_pc_off === 1.U) {
        reg_wdata := pc_off
        reg_wdata_s := 0.S
    } .elsewhen(ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_ld === 1.U) {
        reg_wdata := consistent_reg_wdata_mem(63, 0).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen(ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lw === 1.U && alu_result(2, 0) === 0.U) {
        reg_wdata := reg_wdata_s.asUInt()
        reg_wdata_s := consistent_reg_wdata_mem(31, 0).asSInt()
    } .elsewhen(ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lw === 1.U && alu_result(2, 0) === 4.U) {
        reg_wdata := reg_wdata_s.asUInt()
        reg_wdata_s := consistent_reg_wdata_mem(63, 32).asSInt()
    } .elsewhen(ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lwu === 1.U && alu_result(2, 0) === 0.U) {
        reg_wdata := consistent_reg_wdata_mem(31, 0).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen(ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lwu === 1.U && alu_result(2, 0) === 4.U) {
        reg_wdata := consistent_reg_wdata_mem(63, 32).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lh === 1.U && alu_result(2, 0) === 0.U) {
        reg_wdata_s := consistent_reg_wdata_mem(15, 0).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lh === 1.U && alu_result(2, 0) === 2.U) {
        reg_wdata_s := consistent_reg_wdata_mem(31, 16).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lh === 1.U && alu_result(2, 0) === 4.U) {
        reg_wdata_s := consistent_reg_wdata_mem(47, 32).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lh === 1.U && alu_result(2, 0) === 6.U) {
        reg_wdata_s := consistent_reg_wdata_mem(63, 48).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lhu === 1.U && alu_result(2, 0) === 0.U) {
        reg_wdata := consistent_reg_wdata_mem(15, 0).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lhu === 1.U && alu_result(2, 0) === 2.U) {
        reg_wdata := consistent_reg_wdata_mem(31, 16).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lhu === 1.U && alu_result(2, 0) === 4.U) {
        reg_wdata := consistent_reg_wdata_mem(47, 32).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lhu === 1.U && alu_result(2, 0) === 6.U) {
        reg_wdata := consistent_reg_wdata_mem(63, 48).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 0.U) {
        reg_wdata_s := consistent_reg_wdata_mem(7, 0).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 1.U) {
        reg_wdata_s := consistent_reg_wdata_mem(15, 8).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 2.U) {
        reg_wdata_s := consistent_reg_wdata_mem(23, 16).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 3.U) {
        reg_wdata_s := consistent_reg_wdata_mem(31, 24).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 4.U) {
        reg_wdata_s := consistent_reg_wdata_mem(39, 32).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 5.U) {
        reg_wdata_s := consistent_reg_wdata_mem(47, 40).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 6.U) {
        reg_wdata_s := consistent_reg_wdata_mem(55, 48).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lb === 1.U && alu_result(2, 0) === 7.U) {
        reg_wdata_s := consistent_reg_wdata_mem(63, 56).asSInt()
        reg_wdata := reg_wdata_s.asUInt()
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 0.U) {
        reg_wdata := consistent_reg_wdata_mem(7, 0).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 1.U) {
        reg_wdata := consistent_reg_wdata_mem(15, 8).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 2.U) {
        reg_wdata := consistent_reg_wdata_mem(23, 16).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 3.U) {
        reg_wdata := consistent_reg_wdata_mem(31, 24).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 4.U) {
        reg_wdata := consistent_reg_wdata_mem(39, 32).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 5.U) {
        reg_wdata := consistent_reg_wdata_mem(47, 40).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 6.U) {
        reg_wdata := consistent_reg_wdata_mem(55, 48).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen (ds_fwd_signals_r.reg_wdata_mem === 1.U && ds_fwd_signals_r.inst_lbu === 1.U && alu_result(2, 0) === 7.U) {
        reg_wdata := consistent_reg_wdata_mem(63, 56).asUInt()
        reg_wdata_s := 0.S
    } .elsewhen(ds_fwd_signals_r.reg_wdata_csr === 1.U) {
        reg_wdata := CSR_read_data
        reg_wdata_s := 0.S
    } .otherwise {
        reg_wdata := 0.U
        reg_wdata_s := 0.S
    }

    io.es_to_ws_waddr := ds_fwd_signals_r.reg_waddr
    io.es_to_ws_wdata := reg_wdata
    
    val fs_ex_r = RegInit(0.U(1.W))
    val fs_excode_r = RegInit(0.U(rv_width.W))
    when (es_new_instr === 1.U) {
        fs_ex_r := io.ds_ex
        fs_excode_r := io.ds_excode
    }
    
    when (es_new_instr === 1.U && timer_int === 1.U) {
        timer_int_r := 1.U
    } .elsewhen (es_new_instr === 1.U) {
        timer_int_r := 0.U
    }
    
    // TODO: reconsider the handling of timer interrupt to make sure that we only tag one instruction
    // TODO: reconsider the mstatus, mip and mie csr impact
    //when (/*(es_new_instr === 1.U && inst_reload_no_ex  === 1.U) | */inst_reload_r === 1.U) {
        // we are in the invalid slot,  do not handle any exceptions
    //    es_ex := 0.U
    //} .else
    when (es_valid === 1.U && timer_int_r === 1.U) {
        // This may occur when the current instruction has finished all the operations while still waiting for the next instruction
        // so ex may only occur when this is a normal instruction (not an invalid slot) and when the exception occurs
        // before the instruction comes in
        es_ex := 1.U
    } .elsewhen (/*(es_new_instr === 1.U && io.ds_ex === 1.U) |*/es_valid === 1.U && fs_ex_r === 1.U) {
        // exception from FS: misaligned instruction address
        es_ex := 1.U
    } .elsewhen (es_valid  === 1.U && ds_fwd_signals_r.inst_ecall === 1.U) {
        es_ex := 1.U
    } .elsewhen (es_valid === 1.U && ds_fwd_signals_r.inst_reserved === 1.U) {
        es_ex := 1.U
    } .elsewhen (es_valid === 1.U && es_read_ex === 1.U) {
        es_ex := 1.U
    } .elsewhen (es_valid === 1.U && es_write_ex === 1.U) {
        es_ex := 1.U
    } .elsewhen (es_valid === 1.U && io.illegal_csr) {
        es_ex := 1.U
    } .otherwise {
        es_ex := 0.U
    }
    
    /*when (es_new_instr === 1.U && io.ds_ex === 1.U) {
        // exception from FS: misaligned instruction address
        es_excode := io.ds_excode
    } .else*/when (es_valid === 1.U && fs_ex_r === 1.U) {
        es_excode := fs_excode_r
    } .elsewhen (es_valid  === 1.U && ds_fwd_signals_r.inst_ecall === 1.U) {
        when (io.priv_level === priv_consts.Machine) {
            es_excode := excode_const.MEcall
        } .elsewhen (io.priv_level === priv_consts.Supervisor) {
            es_excode := excode_const.SEcall
        } .otherwise {
            es_excode := excode_const.UEcall
        }
    } .elsewhen (es_valid  === 1.U && ds_fwd_signals_r.inst_reserved === 1.U) {
        es_excode := excode_const.IllegalInstruction
    } .elsewhen (es_valid  === 1.U && es_read_ex === 1.U) {
        es_excode := excode_const.LoadAddrMisaligned
    } .elsewhen (es_valid  === 1.U && es_write_ex === 1.U) {
        es_excode := excode_const.StoreAddrMisaligned
    } .elsewhen (es_valid === 1.U && timer_int_r === 1.U) {
        when (io.priv_level === priv_consts.Machine) {
            es_excode := excode_const.MachineTimerInt
        } .elsewhen (io.priv_level === priv_consts.Supervisor) {
            es_excode := excode_const.SupervisorTimerInt
        } .otherwise {
            es_excode := excode_const.UserTimerInt
        }
    } .elsewhen (es_valid === 1.U && io.illegal_csr) {
        es_excode := excode_const.IllegalInstruction
    } .otherwise {
        es_excode := 0.U
    }
    
    io.ex_valid := es_ex
    
    io.es_forward_valid := es_ready_go
    io.es_forward_waddr := Mux(io.es_to_ws_wen, ds_fwd_signals_r.reg_waddr, 0.U)
    io.es_forward_wdata := reg_wdata
    
}

object CPU_EX extends App {
    chisel3.Driver.execute(args, () => new CPU_EX(64))
}