package Xim

import chisel3._

class CPU_WB(val rv_width: Int = 64) extends Module {
    val io = IO(new Bundle {
        val ws_allow_in = Output(Bool())
        val es_to_ws_valid = Input(Bool())
        val es_pc = Input(UInt(rv_width.W))
        val es_ex = Input(Bool())
        val es_excode = Input(UInt(5.W))
        val es_instr = Input(UInt(32.W))
        
    
        // general purpose registers input
        val wen = Input(Bool())
        val waddr = Input(UInt(5.W))
        val wdata = Input(UInt(rv_width.W))
        
        // general purpose registers output
        val reg_wen = Output(Bool())
        val reg_waddr = Output(UInt(5.W))
        val reg_wdata = Output(UInt(rv_width.W))
        
        // forward result
        val ws_forward_valid = Output(Bool())
        val ws_forward_waddr = Output(UInt(5.W))
        val ws_forward_wdata = Output(UInt(rv_width.W))
        
        // exceptions
        val ex_valid = Input(UInt(1.W))
        val ex_target = Input(UInt(rv_width.W))
    
        // for debug
        val es_reg_wen = Output(UInt(1.W))
        val es_reg_waddr = Output(UInt(5.W))
        val es_reg_wdata = Output(UInt(rv_width.W))
    })
    
    val ws_valid = RegInit(0.B)
    val ws_forward_valid = RegInit(0.B)
    val ws_allow_in = Wire(Bool())
    val ws_ready_go = Wire(Bool())
    val ws_pc = Reg(UInt(rv_width.W))
    val ws_instr = Reg(UInt(32.W))
    val ws_ex = Wire(Bool())
    val ws_excode = Wire(UInt(rv_width.W))
    val ws_ex_r = RegInit(0.B)
    val ws_excode_r = Reg(UInt(5.W))
    
    val ws_new_instr = Wire(Bool())
    val ws_new_instr_r = RegInit(0.B)
    
    val ws_wen_r = RegInit(0.B)
    val ws_waddr_r = Reg(UInt(5.W))
    val ws_wdata_r = Reg(UInt(rv_width.W))
    
    
    when(ws_allow_in) {
        ws_valid := io.es_to_ws_valid
    }
    
    ws_ready_go := 1.B // write back will always finish in one cycle
    
    ws_allow_in := (!ws_valid) || ws_ready_go
    
    io.ws_allow_in := ws_allow_in
    
    ws_new_instr := io.ws_allow_in && io.es_to_ws_valid
    
    ws_new_instr_r := ws_new_instr
    
    ws_ex := io.es_ex
    ws_excode := io.es_excode
    
    when (ws_allow_in) {
        ws_forward_valid := io.es_to_ws_valid
    }
    
    
    when (ws_new_instr) {
        ws_instr := io.es_instr
        ws_pc := io.es_pc
        ws_ex_r := ws_ex
        ws_excode_r := ws_excode
        
        ws_wen_r := io.wen
        ws_waddr_r := io.waddr
        ws_wdata_r := io.wdata
        
    } .otherwise {
        ws_wen_r := 0.B
    }
    
    // redirect register write signals to decode
    
    io.reg_wen := ws_wen_r
    io.reg_waddr := ws_waddr_r
    io.reg_wdata := ws_wdata_r
    
    // forward register value to decode
    io.ws_forward_valid := ws_forward_valid
    io.ws_forward_waddr := Mux(ws_wen_r, ws_waddr_r, 0.U)
    io.ws_forward_wdata := ws_wdata_r
    io.reg_waddr := ws_waddr_r
    io.reg_wdata := ws_wdata_r
    
    io.es_reg_wen := io.reg_wen
    io.es_reg_waddr := io.reg_waddr
    io.es_reg_wdata := io.reg_wdata
}
