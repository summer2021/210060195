package Xim

import chisel3._

class DsToEsBundle extends Bundle {
    val rv_width = 64
    // decoded control signals
    val src1_is_imm = Bool()
    val src1_is_rs1 = Bool()
    val src2_is_rs2 = Bool()
    val src2_is_rs2_self = Bool()
    val src2_is_pc = Bool()
    val src2_is_imm = Bool()
    val src2_is_csr = Bool()
    val src2_is_zero = Bool()
    val inst_imm = UInt(rv_width.W)
    val rs2 = UInt(5.W)
    
    val alu_op_add = Bool()
    val alu_op_sub = Bool()
    val alu_op_slt = Bool()
    val alu_op_sltu = Bool()
    val alu_op_and = Bool()
    val alu_op_or = Bool()
    val alu_op_xor = Bool()
    val alu_op_sll = Bool()
    val alu_op_srl = Bool()
    val alu_op_sra = Bool()
    val alu_op_lui = Bool()
    val alu_op_srl_w = Bool()
    val alu_op_sra_w = Bool()
    val truncatShift = Bool()
    val inst_reserved = Bool()
    
    val inst_lw = Bool()
    val inst_lh = Bool()
    val inst_lhu = Bool()
    val inst_lb = Bool()
    val inst_lbu = Bool()
    val inst_lwu = Bool()
    val inst_ld = Bool()
    val inst_sd = Bool()
    val inst_sw = Bool()
    val inst_sh = Bool()
    val inst_sb = Bool()
    val inst_load = Bool()
    val inst_store = Bool()
    val inst_branch = Bool()
    val inst_b = Bool()
    val inst_ecall = Bool()
    val inst_mret = Bool()
    val inst_sret = Bool()
    val inst_csrrc = Bool()
    val inst_csrrw = Bool()
    val inst_csrrs = Bool()
    val inst_csrrci = Bool()
    val inst_csrrwi = Bool()
    val inst_csrrsi = Bool()
    
    val reg_raddr_1 = UInt(5.W)
    val reg_raddr_2 = UInt(5.W)
    val reg_waddr = UInt(5.W)
    val gr_we = Bool()
    val reg_rdata_1 = UInt(rv_width.W)
    val reg_rdata_2 = UInt(rv_width.W)
    
    val reg_wdata_alu = Bool()
    val reg_wdata_alu_lower = Bool()
    val reg_wdata_mem = Bool()
    val reg_wdata_csr = Bool()
    val reg_wdata_pc_off = Bool()
    val csr_number = UInt(12.W)
}

class CPU_ID(val rv_width: Int = 64) extends Module {
    val io = IO(new Bundle {
        // from fs
        val ds_allow_in = Output(Bool())
        val fs_to_ds_valid = Input(Bool())
        val fs_pc = Input(UInt(rv_width.W))
        val fs_ex = Input(Bool())
        val fs_excode = Input(UInt(5.W))
        val fs_instr = Input(UInt(32.W))
    
        // for branch prediction
        val branch_new_instr = Output(UInt(1.W))
        val branch_br_taken = Output(UInt(1.W))
        val ds_next_branch = Input(UInt(1.W))
        
        // to es
        val ds_to_es_valid = Output(Bool())
        val es_allow_in = Input(Bool())
        val ds_pc = Output(UInt(rv_width.W))
        val ds_ex = Output(Bool())
        val ds_excode = Output(UInt(5.W))
        val ds_instr = Output(UInt(32.W))
        val inst_reload = Output(Bool())
        val ds_to_es_control_signals = Output(new DsToEsBundle)
        
        // general purpose register write signal
        
        // general purpose register forward signal
        // from es
        val es_forward_valid = Input(Bool())
        val es_forward_waddr = Input(UInt(5.W))
        val es_forward_wdata = Input(UInt(rv_width.W))
        // from ws
        val ws_forward_valid = Input(Bool())
        val ws_forward_waddr = Input(UInt(5.W))
        val ws_forward_wdata = Input(UInt(rv_width.W))
        
        // ws writeback signals
        val ws_reg_wen = Input(Bool())
        val ws_reg_waddr = Input(UInt(5.W))
        val ws_reg_wdata = Input(UInt(rv_width.W))
        
        // need to deal with branch here also
    
        val br_valid = Output(UInt(1.W))
        val br_target = Output(UInt(rv_width.W))
        
        // CSR read value
        val CSR_mstatus_tsr = Input(UInt(1.W))
        val CSR_mepc = Input(UInt(rv_width.W))
        val CSR_sepc = Input(UInt(rv_width.W))
        
        // for debugging
        val es_reg_a0 = Output(UInt(rv_width.W))
    })
    
    val ds_valid = RegInit(0.B)
    val ds_allow_in = Wire(Bool())
    val ds_ready_go = Wire(Bool())
    val ds_pc = Reg(UInt(rv_width.W))
    val ds_instr = Reg(UInt(32.W))
    val ds_ex = Wire(Bool())
    val ds_excode = Wire(UInt(rv_width.W))
    val ds_ex_r = RegInit(0.B)
    val ds_excode_r = Reg(UInt(5.W))

    // branch related
    val rs1_equal_rs2 = Wire(UInt(1.W))
    val rs1_less_rs2_unsigned = Wire(UInt(1.W))
    val rs1_less_rs2_signed = Wire(UInt(1.W))
    val br_taken = Wire(UInt(1.W))
    val br_miss = Wire(UInt(1.W))
    val inst_reload = Wire(UInt(1.W))
    val inst_reload_no_ex = Wire(UInt(1.W))
    val beq_taken = Wire(UInt(1.W))
    val bne_taken = Wire(UInt(1.W))
    val blt_taken = Wire(UInt(1.W))
    val bge_taken = Wire(UInt(1.W))
    val bltu_taken = Wire(UInt(1.W))
    val bgeu_taken = Wire(UInt(1.W))
    
    when (ds_allow_in) {
        ds_valid := io.fs_to_ds_valid
    }
    ds_ready_go := ds_valid // decode will always finish in one cycle
    ds_allow_in := !ds_valid || ds_ready_go && io.es_allow_in

    io.ds_allow_in := ds_allow_in

    val ds_new_instr = Wire(Bool())
    val ds_new_instr_r = RegInit(0.B)
    ds_new_instr := io.ds_allow_in && io.fs_to_ds_valid
    
    
    val ds_next_branch = RegInit(0.U(1.W))
    
    ds_ex := io.fs_ex
    ds_excode := io.fs_excode
    when (ds_new_instr && inst_reload === 1.U) {
        ds_instr := 0x00000033.U
        ds_next_branch := io.ds_next_branch
        ds_pc := io.fs_pc
        ds_ex_r := ds_ex
        ds_excode_r := ds_excode
        
        // dont touch register related signals
    } .elsewhen (ds_new_instr) {
        ds_instr := io.fs_instr
        ds_next_branch := io.ds_next_branch
        ds_pc := io.fs_pc
        ds_ex_r := ds_ex
        ds_excode_r := ds_excode
    }
    ds_new_instr_r := ds_new_instr
    
    io.ds_to_es_valid := ds_valid && ds_ready_go
    io.ds_pc := ds_pc
    io.ds_ex := ds_ex
    io.ds_excode := ds_excode
    io.ds_instr := ds_instr
    
    when (io.fs_ex) {
        ds_ex := io.fs_ex
        ds_excode := io.fs_excode
    }
    
    val rs1_read_valid = Wire(Bool())
    val rs2_read_valid = Wire(Bool())
    
    val rs1_read_zero = Wire(Bool())
    val rs2_read_zero = Wire(Bool())
    
    // decode related
    val inst_load = Wire(UInt(1.W))
    val inst_store = Wire(UInt(1.W))
    val inst_branch = Wire(UInt(1.W))

    lazy val inst_lui = Wire(UInt(1.W))
    lazy val inst_auipc = Wire(UInt(1.W))
    lazy val inst_jal = Wire(UInt(1.W))
    lazy val inst_jalr = Wire(UInt(1.W))
    lazy val inst_beq = Wire(UInt(1.W))
    lazy val inst_bne = Wire(UInt(1.W))
    lazy val inst_blt = Wire(UInt(1.W))
    lazy val inst_bge = Wire(UInt(1.W))
    lazy val inst_bltu = Wire(UInt(1.W))
    lazy val inst_bgeu = Wire(UInt(1.W))
    lazy val inst_lb = Wire(UInt(1.W))
    lazy val inst_lh = Wire(UInt(1.W))
    lazy val inst_lw = Wire(UInt(1.W))
    lazy val inst_lwu = Wire(UInt(1.W))
    lazy val inst_ld = Wire(UInt(1.W))
    lazy val inst_lbu = Wire(UInt(1.W))
    lazy val inst_lhu = Wire(UInt(1.W))
    lazy val inst_sb = Wire(UInt(1.W))
    lazy val inst_sh = Wire(UInt(1.W))
    lazy val inst_sw = Wire(UInt(1.W))
    lazy val inst_sd = Wire(UInt(1.W))
    lazy val inst_addi = Wire(UInt(1.W))
    lazy val inst_addiw = Wire(UInt(1.W))
    lazy val inst_slliw = Wire(UInt(1.W))
    lazy val inst_srliw = Wire(UInt(1.W))
    lazy val inst_sraiw = Wire(UInt(1.W))
    lazy val inst_addw = Wire(UInt(1.W))
    lazy val inst_subw = Wire(UInt(1.W))
    lazy val inst_sllw = Wire(UInt(1.W))
    lazy val inst_srlw = Wire(UInt(1.W))
    lazy val inst_sraw = Wire(UInt(1.W))
    lazy val inst_slti = Wire(UInt(1.W))
    lazy val inst_sltiu = Wire(UInt(1.W))
    lazy val inst_xori = Wire(UInt(1.W))
    lazy val inst_ori = Wire(UInt(1.W))
    lazy val inst_andi = Wire(UInt(1.W))
    lazy val inst_slli = Wire(UInt(1.W))
    lazy val inst_srli = Wire(UInt(1.W))
    lazy val inst_srai = Wire(UInt(1.W))
    lazy val inst_add = Wire(UInt(1.W))
    lazy val inst_sub = Wire(UInt(1.W))
    lazy val inst_sll = Wire(UInt(1.W))
    lazy val inst_slt = Wire(UInt(1.W))
    lazy val inst_sltu = Wire(UInt(1.W))
    lazy val inst_xor = Wire(UInt(1.W))
    lazy val inst_srl = Wire(UInt(1.W))
    lazy val inst_sra = Wire(UInt(1.W))
    lazy val inst_or = Wire(UInt(1.W))
    lazy val inst_and = Wire(UInt(1.W))
    lazy val inst_fence = Wire(UInt(1.W))
    lazy val inst_ecall = Wire(UInt(1.W))
    lazy val inst_ebreak = Wire(UInt(1.W))
    lazy val inst_csrrw = Wire(UInt(1.W))
    lazy val inst_csrrs = Wire(UInt(1.W))
    lazy val inst_csrrc = Wire(UInt(1.W))
    lazy val inst_csrrwi = Wire(UInt(1.W))
    lazy val inst_csrrsi = Wire(UInt(1.W))
    lazy val inst_csrrci = Wire(UInt(1.W))
    lazy val inst_fence_i = Wire(UInt(1.W))
    lazy val inst_mret = Wire(UInt(1.W))
    lazy val inst_sret = Wire(UInt(1.W))
    lazy val inst_uret = Wire(UInt(1.W))
    val inst_reserved = Wire(UInt(1.W)) // reserved instruction
    val es_ecall = Wire(UInt(1.W))

    // inst coding zone
    val opcode = (() => (Wire(UInt(7.W))))()
    val opcode_d = (() => (Wire(UInt(128.W))))()
    val funct7 = (() => (Wire(UInt(7.W))))()
    val funct7_d = (() => (Wire(UInt(128.W))))()
    val funct3 = (() => (Wire(UInt(3.W))))()
    val funct3_d = (() => (Wire(UInt(8.W))))()
    val rs1 = (() => (Wire(UInt(5.W))))()
    val rs2 = (() => (Wire(UInt(5.W))))()
    val rd = (() => (Wire(UInt(5.W))))()

    val I_imm = (() => (Wire(SInt(rv_width.W))))()  // sign extend
    val S_imm = (() => (Wire(SInt(rv_width.W))))()  // sign extend
    val B_imm = (() => (Wire(SInt(rv_width.W))))()  // sign extend
    val U_imm = (() => (Wire(UInt(rv_width.W))))()  // sign extend, imm to upper
    val U_imm_s = (() => (Wire(SInt(rv_width.W))))()
    val J_imm = (() => (Wire(SInt(rv_width.W))))()  // sign extend
    val Csr_imm = (() => (Wire(UInt(rv_width.W))))()  // zero extend
    val Csr_imm_tmp = (() => (Wire(UInt(rv_width.W))))()  // make sure cast to rv_width bit
    val Csr_num = (() => (Wire(UInt(12.W))))()  // exactly
    val I_imm_b = (() => (Wire(new I_imm_bundle)))()
    val S_imm_b = (() => (Wire(new S_imm_bundle)))()
    val B_imm_b = (() => (Wire(new B_imm_bundle)))()
    val U_imm_b = (() => (Wire(new U_imm_bundle)))()
    val J_imm_b = (() => (Wire(new J_imm_bundle)))()
    val inst_imm = (() => (Wire(UInt(rv_width.W))))()
    val inst_i = Wire(UInt(1.W))
    val inst_s = Wire(UInt(1.W))
    val inst_b = Wire(UInt(1.W))
    val inst_u = Wire(UInt(1.W))
    val inst_j = Wire(UInt(1.W))
    val inst_r = Wire(UInt(1.W))
    
    rs1_read_zero := rs1 === 0.U
    rs2_read_zero := rs2 === 0.U
    
    rs1_read_valid := !(rs1_read_zero | inst_lui | inst_jal | inst_ecall | inst_ebreak | inst_fence | inst_fence_i)
    rs2_read_valid := !(rs2_read_zero | inst_load | inst_lui | inst_jal | inst_ecall | inst_ebreak | inst_fence | inst_fence_i | inst_addi | inst_addiw | inst_slti |
      inst_sltiu | inst_xori | inst_ori | inst_andi | inst_slli | inst_srli | inst_srai | inst_csrrwi | inst_csrrsi | inst_csrrci | inst_slliw | inst_srliw | inst_sraiw)
    
    val rs1_read_conflict = ((rs1 === io.es_forward_waddr && !io.es_forward_valid) | (rs1 === io.ws_forward_waddr && !io.ws_forward_valid)) && rs1_read_valid
    val rs2_read_conflict = ((rs2 === io.es_forward_waddr && !io.es_forward_valid) | (rs2 === io.ws_forward_waddr && !io.ws_forward_valid)) && rs2_read_valid
    

    
    inst_i := inst_uret | inst_sret | inst_mret | inst_jalr | inst_lb | inst_lh | inst_lw | inst_lwu | inst_ld | inst_lbu | inst_lhu |
      inst_addi | inst_addiw | inst_slti | inst_sltiu | inst_xori | inst_ori | inst_andi | inst_fence_i
    inst_s := inst_sb | inst_sh | inst_sw | inst_sd
    inst_b := inst_beq | inst_bne | inst_blt | inst_bge | inst_bltu | inst_bgeu
    inst_u := inst_lui | inst_auipc
    inst_j := inst_jal
    inst_r := inst_slli | inst_slliw | inst_srli | inst_srliw | inst_srai | inst_sraiw |
      inst_add | inst_addw | inst_sub | inst_subw | inst_sll | inst_sllw | inst_slt | inst_sltu | inst_xor | inst_srl | inst_srlw |
      inst_sra | inst_sraw | inst_or | inst_and
    
    when(inst_i === 1.U) {
        inst_imm := I_imm.asUInt
    }.elsewhen(inst_s === 1.U) {
        inst_imm := S_imm.asUInt
    }.elsewhen(inst_b === 1.U) {
        inst_imm := B_imm.asUInt
    }.elsewhen(inst_u === 1.U) {
        inst_imm := U_imm
    }.elsewhen(inst_j === 1.U) {
        inst_imm := J_imm.asUInt
    } .elsewhen ((inst_csrrci | inst_csrrwi | inst_csrrsi) === 1.U) {
        inst_imm := Csr_imm
    } .otherwise {
        inst_imm := 0.U
    }
    
    opcode := ds_instr(6, 0)
    funct7 := ds_instr(31, 25)
    funct3 := ds_instr(14, 12)
    rs1 := ds_instr(19, 15)
    rs2 := ds_instr(24, 20)
    rd := ds_instr(11, 7)
    I_imm := (I_imm_b.asUInt).asSInt()
    S_imm := (S_imm_b.asUInt).asSInt()
    B_imm := (B_imm_b.asUInt).asSInt()
    U_imm_s := (U_imm_b.asUInt).asSInt()
    U_imm := U_imm_s.asUInt()
    J_imm := (J_imm_b.asUInt).asSInt()
    
    when (inst_csrrci === 1.U) {
        Csr_imm := ~Csr_imm_tmp
    } .otherwise {
        Csr_imm := Csr_imm_tmp
    }
    
    Csr_imm_tmp := rs1.asUInt()
    
    I_imm_b.inst_31 := ds_instr(31)
    I_imm_b.inst_30_25 := ds_instr(30, 25)
    I_imm_b.inst_24_21 := ds_instr(24, 21)
    I_imm_b.inst_20 := ds_instr(20)
    
    S_imm_b.inst_31 := ds_instr(31)
    S_imm_b.inst_30_25 := ds_instr(30, 25)
    S_imm_b.inst_11_8 := ds_instr(11, 8)
    S_imm_b.inst_7 := ds_instr(7)
    
    B_imm_b.inst_31 := ds_instr(31)
    B_imm_b.inst_7 := ds_instr(7)
    B_imm_b.inst_30_25 := ds_instr(30, 25)
    B_imm_b.inst_11_8 := ds_instr(11, 8)
    B_imm_b.zero := 0.U
    
    U_imm_b.inst_31 := ds_instr(31)
    U_imm_b.inst_30_20 := ds_instr(30, 20)
    U_imm_b.inst_19_12 := ds_instr(19, 12)
    U_imm_b.zero := 0.U
    
    J_imm_b.inst_31 := ds_instr(31)
    J_imm_b.inst_19_12 := ds_instr(19, 12)
    J_imm_b.inst_20 := ds_instr(20)
    J_imm_b.inst_30_25 := ds_instr(30, 25)
    J_imm_b.inst_24_21 := ds_instr(24, 21)
    J_imm_b.zero := 0.U
    
    Csr_num := ds_instr(31, 20)
    io.ds_to_es_control_signals.csr_number := Csr_num
    
    
    val opcode_decoder: decoder_7_128 = Module(new decoder_7_128)
    val funct7_decoder: decoder_7_128 = Module(new decoder_7_128)
    val funct3_decoder: decoder_3_8 = Module(new decoder_3_8)
    
    opcode_decoder.io.in := opcode
    opcode_d := opcode_decoder.io.out
    
    funct7_decoder.io.in := funct7
    funct7_d := funct7_decoder.io.out
    
    funct3_decoder.io.in := funct3
    funct3_d := funct3_decoder.io.out
    
    inst_mret := opcode_d(0x73) === 1.U && I_imm === 0x302.S && funct3_d(0) === 1.U
    inst_sret := opcode_d(0x73) === 1.U && I_imm === 0x102.S && funct3_d(0) === 1.U
    inst_uret := opcode_d(0x73) === 1.U && I_imm === 0x002.S && funct3_d(0) === 1.U
    inst_lui := (opcode_d(0x37) === 1.U)
    inst_auipc := (opcode_d(0x17) === 1.U)
    inst_jal := (opcode_d(0x6f) === 1.U)
    inst_jalr := (opcode_d(0x67) === 1.U) && (funct3_d(0) === 1.U)
    inst_beq := (opcode_d(0x63) === 1.U) && (funct3_d(0) === 1.U)
    inst_bne := (opcode_d(0x63) === 1.U) && (funct3_d(1) === 1.U)
    inst_blt := (opcode_d(0x63) === 1.U) && (funct3_d(4) === 1.U)
    inst_bge := (opcode_d(0x63) === 1.U) && (funct3_d(5) === 1.U)
    inst_bltu := (opcode_d(0x63) === 1.U) && (funct3_d(6) === 1.U)
    inst_bgeu := (opcode_d(0x63) === 1.U) && (funct3_d(7) === 1.U)
    inst_lb := (opcode_d(0x3) === 1.U) && (funct3_d(0) === 1.U)
    inst_lh := (opcode_d(0x3) === 1.U) && (funct3_d(1) === 1.U)
    inst_lw := (opcode_d(0x3) === 1.U) && (funct3_d(2) === 1.U)
    inst_lwu := (opcode_d(0x3) === 1.U) && (funct3_d(6) === 1.U)
    inst_ld := (opcode_d(0x3) === 1.U) && (funct3_d(3) === 1.U)
    inst_lbu := (opcode_d(0x3) === 1.U) && (funct3_d(4) === 1.U)
    inst_lhu := (opcode_d(0x3) === 1.U) && (funct3_d(5) === 1.U)
    inst_sb := (opcode_d(0x23) === 1.U) && (funct3_d(0) === 1.U)
    inst_sh := (opcode_d(0x23) === 1.U) && (funct3_d(1) === 1.U)
    inst_sw := (opcode_d(0x23) === 1.U) && (funct3_d(2) === 1.U)
    inst_sd := (opcode_d(0x23) === 1.U) && (funct3_d(3) === 1.U)
    inst_addi := (opcode_d(0x13) === 1.U) && (funct3_d(0) === 1.U)
    inst_addiw := (opcode_d(0x1b) === 1.U) && (funct3_d(0) === 1.U)
    inst_slliw := (opcode_d(0x1b) === 1.U) && (funct3_d(1) === 1.U) && (funct7_d(0) === 1.U)
    inst_srliw := (opcode_d(0x1b) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0) === 1.U)
    inst_sraiw := (opcode_d(0x1b) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0x20) === 1.U)
    inst_addw := (opcode_d(0x3b) === 1.U) && (funct3_d(0) === 1.U) && (funct7_d(0) === 1.U)
    inst_subw := (opcode_d(0x3b) === 1.U) && (funct3_d(0) === 1.U) && (funct7_d(0x20) === 1.U)
    inst_sllw := (opcode_d(0x3b) === 1.U) && (funct3_d(1) === 1.U) && (funct7_d(0) === 1.U)
    inst_srlw := (opcode_d(0x3b) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0) === 1.U)
    inst_sraw := (opcode_d(0x3b) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0x20) === 1.U)
    inst_slti := (opcode_d(0x13) === 1.U) && (funct3_d(2) === 1.U)
    inst_sltiu := (opcode_d(0x13) === 1.U) && (funct3_d(3) === 1.U)
    inst_xori := (opcode_d(0x13) === 1.U) && (funct3_d(4) === 1.U)
    inst_ori := (opcode_d(0x13) === 1.U) && (funct3_d(6) === 1.U)
    inst_andi := (opcode_d(0x13) === 1.U) && (funct3_d(7) === 1.U)
    inst_slli := (opcode_d(0x13) === 1.U) && (funct3_d(1) === 1.U) && (funct7_d(0x0) | funct7_d(0x1) === 1.U)
    inst_srli := (opcode_d(0x13) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0x0) | funct7_d(0x1) === 1.U)
    inst_srai := (opcode_d(0x13) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0x20) === 1.U)
    inst_add := (opcode_d(0x33) === 1.U) && (funct3_d(0) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_sub := (opcode_d(0x33) === 1.U) && (funct3_d(0) === 1.U) && (funct7_d(0x20) === 1.U)
    inst_sll := (opcode_d(0x33) === 1.U) && (funct3_d(1) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_slt := (opcode_d(0x33) === 1.U) && (funct3_d(2) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_sltu := (opcode_d(0x33) === 1.U) && (funct3_d(3) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_xor := (opcode_d(0x33) === 1.U) && (funct3_d(4) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_srl := (opcode_d(0x33) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_sra := (opcode_d(0x33) === 1.U) && (funct3_d(5) === 1.U) && (funct7_d(0x20) === 1.U)
    inst_or := (opcode_d(0x33) === 1.U) && (funct3_d(6) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_and := (opcode_d(0x33) === 1.U) && (funct3_d(7) === 1.U) && (funct7_d(0x0) === 1.U)
    inst_fence := (opcode_d(0xf) === 1.U) && (funct3_d(0) === 1.U)
    inst_ecall := (opcode_d(0x73) === 1.U) && (funct3_d(0) === 1.U) && (funct7_d(0x0) === 1.U) && (rs1 === 0.U) && (rs2 === 0.U) && (rd === 0.U)
    inst_ebreak := (opcode_d(0x73) === 1.U) && (funct3_d(0) === 1.U) && (funct7_d(0x0) === 1.U) && (rs1 === 0.U) && (rs2 === 1.U) && (rd === 0.U)
    inst_fence_i := (opcode_d(0xf) === 1.U) && (funct3_d(1) === 1.U)
    inst_csrrw := (opcode_d(0x73) === 1.U) && (funct3_d(1) === 1.U)
    inst_csrrs := (opcode_d(0x73) === 1.U) && (funct3_d(2) === 1.U)
    inst_csrrc := (opcode_d(0x73) === 1.U) && (funct3_d(3) === 1.U)
    inst_csrrwi := (opcode_d(0x73) === 1.U) && (funct3_d(5) === 1.U)
    inst_csrrsi := (opcode_d(0x73) === 1.U) && (funct3_d(6) === 1.U)
    inst_csrrci := (opcode_d(0x73) === 1.U) && (funct3_d(7) === 1.U)
    
    es_ecall := inst_ecall
    
    inst_reserved := !(
      // RV64I
      inst_lui === 1.U || inst_auipc === 1.U || inst_jal === 1.U ||
        inst_jalr === 1.U || inst_beq === 1.U || inst_bne === 1.U ||
        inst_blt === 1.U || inst_bge === 1.U || inst_bltu === 1.U ||
        inst_bgeu === 1.U || inst_lb === 1.U || inst_lh === 1.U ||
        inst_lw === 1.U || inst_lbu === 1.U || inst_lhu === 1.U ||
        inst_sb === 1.U || inst_sh === 1.U || inst_sw === 1.U ||
        inst_lwu === 1.U || inst_ld === 1.U || inst_sd === 1.U ||
        inst_addi === 1.U || inst_slti === 1.U || inst_sltiu === 1.U ||
        inst_xori === 1.U || inst_ori === 1.U || inst_andi === 1.U ||
        inst_slli === 1.U || inst_srli === 1.U || inst_srai === 1.U ||
        inst_add === 1.U || inst_sub === 1.U || inst_sll === 1.U ||
        inst_slt === 1.U || inst_sltu === 1.U || inst_xor === 1.U ||
        inst_srl === 1.U || inst_sra === 1.U || inst_or === 1.U ||
        inst_and === 1.U || inst_fence === 1.U || inst_ecall === 1.U ||
        inst_ebreak === 1.U ||
        inst_addiw === 1.U || inst_slliw === 1.U || inst_srliw === 1.U ||
        inst_sraiw === 1.U || inst_addw === 1.U || inst_subw === 1.U ||
        inst_sllw === 1.U || inst_srlw === 1.U || inst_sraw === 1.U ||
        // RV64Zifencei
        inst_fence_i === 1.U ||
        // RV64 Machine
        inst_mret === 1.U ||
        // RV64 Supervisor
        (inst_sret === 1.U && io.CSR_mstatus_tsr === 0.U) ||
        // RV64 User
        inst_uret === 1.U ||
        // RV64Zicsr
        inst_csrrw === 1.U || inst_csrrs === 1.U || inst_csrrc === 1.U || inst_csrrc === 1.U || inst_csrrwi === 1.U || inst_csrrsi === 1.U || inst_csrrci === 1.U
      )
    
    val es_src1_rs1 = Wire(UInt(1.W))
    val es_src1_imm = Wire(UInt(1.W))
    val es_src2_rs2 = Wire(UInt(1.W))
    val es_src2_rs2_self = Wire(UInt(1.W))
    val es_src2_pc = Wire(UInt(1.W))
    val es_src2_imm = Wire(UInt(1.W))
    val es_src2_csr = Wire(UInt(1.W))
    val es_src2_zero = Wire(UInt(1.W))
    val es_alu_op_add = Wire(UInt(1.W))
    val es_alu_op_sub = Wire(UInt(1.W))
    val es_alu_op_slt = Wire(UInt(1.W))
    val es_alu_op_sltu = Wire(UInt(1.W))
    val es_alu_op_and = Wire(UInt(1.W))
    val es_alu_op_nor = Wire(UInt(1.W))
    val es_alu_op_or = Wire(UInt(1.W))
    val es_alu_op_xor = Wire(UInt(1.W))
    val es_alu_op_sll = Wire(UInt(1.W))
    val es_alu_op_srl = Wire(UInt(1.W))
    val es_alu_op_sra = Wire(UInt(1.W))
    val es_alu_op_lui = Wire(UInt(1.W))
    val es_alu_op_srl_w = Wire(UInt(1.W))
    val es_alu_op_sra_w = Wire(UInt(1.W))
    
    // Write back related
    val reg_file = Module(new RegFile(2))
    val reg_wen = Wire(UInt(1.W))
    val reg_waddr = Wire(UInt(5.W))
    val reg_raddr_1 = Wire(UInt(5.W))
    val reg_raddr_2 = Wire(UInt(5.W))
    // val reg_raddr = Wire(new regfile_raddr)
    val reg_rdata_1 = Wire(UInt(rv_width.W))
    val reg_rdata_2 = Wire(UInt(rv_width.W))
    val gr_we = Wire(UInt(1.W))
    
    
    
    reg_raddr_1 := rs1
    reg_raddr_2 := rs2
    reg_waddr := rd
    reg_wen := gr_we
    
    // reg file connection
    reg_file.io.wen := io.ws_reg_wen
    reg_file.io.waddr := io.ws_reg_waddr
    reg_file.io.wdata := io.ws_reg_wdata
    reg_file.io.raddr(0) := reg_raddr_1
    reg_file.io.raddr(1) := reg_raddr_2
    
    io.es_reg_a0 := reg_file.io.debug_a0
    
    when (io.es_forward_valid && rs1 === io.es_forward_waddr && !rs1_read_zero) {
        reg_rdata_1 := io.es_forward_wdata
    } .elsewhen (io.ws_forward_valid & rs1 === io.ws_forward_waddr & !rs1_read_zero) {
        reg_rdata_1 := io.es_forward_wdata
    } .otherwise {
        reg_rdata_1 := reg_file.io.rdata(0)
    }
    
    when (io.es_forward_valid && rs2 === io.es_forward_waddr && rs2_read_zero) {
        reg_rdata_2 := io.es_forward_wdata
    } .elsewhen (io.ws_forward_valid && rs2 === io.ws_forward_waddr && rs2_read_zero) {
        reg_rdata_2 := io.ws_forward_wdata
    } .otherwise {
        reg_rdata_2 := reg_file.io.rdata(1)
    }
    
    
    gr_we := inst_lui | inst_auipc | inst_jal | inst_jalr | inst_lb | inst_lh | inst_lw | inst_lwu | inst_ld | inst_lbu | inst_lhu | inst_addi | inst_addiw |
      inst_slti | inst_sltiu | inst_xori | inst_ori | inst_andi | inst_slli | inst_slliw | inst_srli | inst_srliw | inst_srai | inst_sraiw | inst_add | inst_addw |
      inst_sub | inst_subw | inst_sll | inst_sllw | inst_slt | inst_sltu | inst_xor | inst_srl | inst_srlw | inst_sra | inst_sraw | inst_or | inst_and | inst_csrrc |
      inst_csrrci | inst_csrrs | inst_csrrsi | inst_csrrw | inst_csrrwi
    
    
    
    es_src1_imm := inst_auipc | inst_jal | inst_beq | inst_bne | inst_blt | inst_bge | inst_bltu | inst_bgeu |
      inst_csrrwi | inst_csrrsi | inst_csrrci
    es_src1_rs1 := inst_jalr |
      inst_lb | inst_lh | inst_lw | inst_lwu | inst_ld | inst_lbu | inst_lhu | inst_sb | inst_sh | inst_sw | inst_sd |
      inst_addi | inst_addiw | inst_add | inst_addw | inst_sub | inst_subw | inst_slti | inst_slt | inst_sltiu | inst_sltu | inst_andi | inst_and | inst_ori | inst_or |
      inst_xori | inst_xor | inst_slli | inst_slliw | inst_sll | inst_sllw | inst_srli | inst_srliw | inst_srl | inst_srlw | inst_srai | inst_sraiw | inst_sra | inst_sraw | inst_lui | // LUI will not use src1
      inst_csrrs | inst_csrrw | inst_csrrc
    es_src2_rs2 := inst_add | inst_addw | inst_sub | inst_subw | inst_slt | inst_sltu | inst_and | inst_or | inst_xor | inst_sll | inst_sllw | inst_srl | inst_srlw | inst_sra | inst_sraw
    es_src2_rs2_self := inst_slli | inst_slliw | inst_srli | inst_srliw | inst_srai | inst_sraiw
    es_src2_pc := inst_auipc | inst_jal | inst_beq | inst_bne | inst_blt | inst_bge | inst_bltu | inst_bgeu
    es_src2_imm := inst_jalr | inst_lb | inst_lh | inst_lw | inst_lwu | inst_ld | inst_lbu | inst_lhu | inst_sb | inst_sh | inst_sw | inst_sd |
      inst_addi | inst_addiw | inst_slti | inst_sltiu | inst_andi | inst_ori | inst_xori | inst_lui
    es_src2_csr := inst_csrrc | inst_csrrs | inst_csrrci | inst_csrrsi
    es_src2_zero := inst_csrrw | inst_csrrwi
    
    es_alu_op_add := (inst_auipc | inst_jal | inst_jalr | inst_beq | inst_bne | inst_blt | inst_bge |
      inst_bltu | inst_bgeu | inst_lb | inst_lh | inst_lw | inst_lwu | inst_ld | inst_lbu | inst_lhu |
      inst_sb | inst_sh | inst_sw | inst_sd | inst_addi | inst_addiw | inst_add | inst_addw | inst_csrrw | inst_csrrwi)
    es_alu_op_sub := inst_sub | inst_subw
    es_alu_op_slt := inst_slti | inst_slt
    es_alu_op_sltu := inst_sltiu | inst_sltu
    es_alu_op_and := inst_andi | inst_and | inst_csrrc | inst_csrrci // csrrc and csrrci: ~imm & csr
    es_alu_op_nor := 0.U // unused for risc-v
    es_alu_op_or := inst_ori | inst_or | inst_csrrs | inst_csrrsi
    es_alu_op_xor := inst_xori | inst_xor
    es_alu_op_sll := inst_slli | inst_slliw | inst_sll | inst_sllw
    es_alu_op_srl := inst_srli | inst_srl
    es_alu_op_sra := inst_srai | inst_sra
    es_alu_op_lui := inst_lui
    es_alu_op_srl_w := inst_srliw | inst_srlw
    es_alu_op_sra_w := inst_sraiw | inst_sraw
    
    val truncatShift = Wire(UInt(1.W))
    truncatShift := inst_sllw | inst_srlw | inst_sraw
    
    // Data load and store related
    inst_load := inst_lw | inst_lh | inst_lhu | inst_lb | inst_lbu | inst_lwu | inst_ld
    inst_store := inst_sd | inst_sw | inst_sh | inst_sb
    inst_branch := inst_b | inst_jal | inst_jalr // jump instruction is dealed with the same schema
    

    rs1_equal_rs2 := reg_rdata_1 === reg_rdata_2
    rs1_less_rs2_unsigned := reg_rdata_1.asUInt() < reg_rdata_2.asUInt()
    rs1_less_rs2_signed := reg_rdata_1.asSInt() < reg_rdata_2.asSInt()
    
    beq_taken := inst_beq === 1.U && rs1_equal_rs2 === 1.U
    bne_taken := inst_bne === 1.U && rs1_equal_rs2 === 0.U
    blt_taken := inst_blt === 1.U && rs1_less_rs2_signed === 1.U
    bge_taken := inst_bge === 1.U && rs1_less_rs2_signed === 0.U
    bltu_taken := inst_bltu === 1.U && rs1_less_rs2_unsigned === 1.U
    bgeu_taken := inst_bgeu === 1.U && rs1_less_rs2_unsigned === 0.U
    
    val branch_taken_condition = Wire(UInt(1.W))
    val inst_reload_r = RegInit(0.U(1.W))

    branch_taken_condition := beq_taken | bne_taken | blt_taken | bge_taken | bltu_taken | bgeu_taken
    br_taken := inst_b & branch_taken_condition & (ds_next_branch === 0.U)
    br_miss := (inst_b) & (~branch_taken_condition) & (ds_next_branch === 1.U) & (~inst_j)
    
    // TODO: consider ex related
    inst_reload_no_ex := br_taken | br_miss | /*inst_jal |*/ inst_jalr | inst_mret | inst_sret | inst_uret
    inst_reload := inst_reload_no_ex | ds_ex
    io.br_valid := inst_reload & ~inst_reload_r
    
    val br_target = Wire(UInt(rv_width.W))
    when (inst_jalr === 1.U) {
        br_target := reg_rdata_1 + inst_imm
    } .otherwise {
        br_target := ds_pc + inst_imm
    }
    
    when(br_taken === 1.U /*|| inst_jal === 1.U*/ || inst_jalr === 1.U) {
        io.br_target := br_target
        // printf(p"Branch taken, target = ${io.br_target}")
    } .elsewhen (br_miss === 1.U) {
        io.br_target := ds_pc + 4.U
    } .elsewhen (inst_mret === 1.U) {
        io.br_target := io.CSR_mepc
    } .elsewhen (inst_sret === 1.U) {
        io.br_target := io.CSR_sepc
    } .otherwise {
        io.br_target := 0.U
    }

    
    when(ds_new_instr) {
        inst_reload_r := inst_reload // only as a debug method
    }
    
    io.inst_reload := inst_reload_r
    
    
    // wdata related signals
    val reg_wdata_alu = Wire(UInt(1.W))
    val reg_wdata_alu_lower = Wire(UInt(1.W))
    val reg_wdata_mem = Wire(UInt(1.W))
    val reg_wdata_csr = Wire(UInt(1.W))
    val reg_wdata_pc_off = Wire(UInt(1.W))
    
    reg_wdata_alu := inst_lui | inst_auipc | inst_addi | inst_addiw | inst_slti | inst_sltiu | inst_xori |
      inst_ori | inst_andi | inst_slli | inst_slliw | inst_srli | inst_srliw | inst_srai | inst_sraiw | inst_add | inst_addw | inst_sub | inst_subw | inst_sll | inst_sllw | inst_slt | inst_sltu |
      inst_xor | inst_srl | inst_sra | inst_srlw | inst_sraw | inst_or | inst_and
    
    reg_wdata_alu_lower := inst_addiw | inst_slliw | inst_srliw | inst_sraiw | inst_addw | inst_subw | inst_sllw | inst_srlw | inst_sraw
    
    reg_wdata_mem := inst_lb | inst_lh | inst_lw | inst_lbu | inst_lhu | inst_lwu | inst_ld
    reg_wdata_csr := inst_csrrc | inst_csrrci | inst_csrrs | inst_csrrsi | inst_csrrw | inst_csrrwi
    
    reg_wdata_pc_off := inst_jal | inst_jalr
    
    
    // connect control signals to the following stage
    io.ds_to_es_control_signals.src1_is_imm := es_src1_imm
    io.ds_to_es_control_signals.src1_is_rs1 := es_src1_rs1
    io.ds_to_es_control_signals.src2_is_rs2 := es_src2_rs2
    io.ds_to_es_control_signals.src2_is_rs2_self := es_src2_rs2_self
    io.ds_to_es_control_signals.src2_is_pc := es_src2_pc
    io.ds_to_es_control_signals.src2_is_imm := es_src2_imm
    io.ds_to_es_control_signals.src2_is_csr := es_src2_csr
    io.ds_to_es_control_signals.src2_is_zero := es_src2_zero
    io.ds_to_es_control_signals.inst_imm := inst_imm
    io.ds_to_es_control_signals.rs2 := rs2
    
    io.ds_to_es_control_signals.alu_op_add := es_alu_op_add
    io.ds_to_es_control_signals.alu_op_sub := es_alu_op_sub
    io.ds_to_es_control_signals.alu_op_slt := es_alu_op_slt
    io.ds_to_es_control_signals.alu_op_sltu := es_alu_op_sltu
    io.ds_to_es_control_signals.alu_op_and := es_alu_op_and
    io.ds_to_es_control_signals.alu_op_or := es_alu_op_or
    io.ds_to_es_control_signals.alu_op_xor := es_alu_op_xor
    io.ds_to_es_control_signals.alu_op_sll := es_alu_op_sll
    io.ds_to_es_control_signals.alu_op_srl := es_alu_op_srl
    io.ds_to_es_control_signals.alu_op_sra := es_alu_op_sra
    io.ds_to_es_control_signals.alu_op_lui := es_alu_op_lui
    io.ds_to_es_control_signals.alu_op_srl_w := es_alu_op_srl_w
    io.ds_to_es_control_signals.alu_op_sra_w := es_alu_op_sra_w
    io.ds_to_es_control_signals.truncatShift := truncatShift
    io.ds_to_es_control_signals.inst_reserved := inst_reserved
    io.ds_to_es_control_signals.inst_lw := inst_lw
    io.ds_to_es_control_signals.inst_lh := inst_lh
    io.ds_to_es_control_signals.inst_lhu := inst_lhu
    io.ds_to_es_control_signals.inst_lb := inst_lb
    io.ds_to_es_control_signals.inst_lbu := inst_lbu
    io.ds_to_es_control_signals.inst_lwu := inst_lwu
    io.ds_to_es_control_signals.inst_ld := inst_ld
    io.ds_to_es_control_signals.inst_sd := inst_sd
    io.ds_to_es_control_signals.inst_sw := inst_sw
    io.ds_to_es_control_signals.inst_sh := inst_sh
    io.ds_to_es_control_signals.inst_sb := inst_sb
    io.ds_to_es_control_signals.inst_load := inst_load
    io.ds_to_es_control_signals.inst_store := inst_store
    io.ds_to_es_control_signals.inst_branch := inst_branch
    io.ds_to_es_control_signals.inst_b := inst_b
    io.ds_to_es_control_signals.inst_ecall := inst_ecall
    io.ds_to_es_control_signals.gr_we := gr_we
    io.ds_to_es_control_signals.inst_mret := inst_mret
    io.ds_to_es_control_signals.inst_sret := inst_sret
    io.ds_to_es_control_signals.inst_csrrs := inst_csrrs
    io.ds_to_es_control_signals.inst_csrrc := inst_csrrc
    io.ds_to_es_control_signals.inst_csrrw := inst_csrrw
    io.ds_to_es_control_signals.inst_csrrsi := inst_csrrsi
    io.ds_to_es_control_signals.inst_csrrci := inst_csrrci
    io.ds_to_es_control_signals.inst_csrrwi := inst_csrrwi

    io.ds_to_es_control_signals.reg_rdata_1 := reg_rdata_1
    io.ds_to_es_control_signals.reg_rdata_2 := reg_rdata_2
    io.ds_to_es_control_signals.reg_raddr_1 := reg_raddr_1
    io.ds_to_es_control_signals.reg_raddr_2 := reg_raddr_2
    io.ds_to_es_control_signals.reg_waddr := reg_waddr

    io.ds_to_es_control_signals.reg_wdata_csr := reg_wdata_csr
    io.ds_to_es_control_signals.reg_wdata_alu_lower := reg_wdata_alu_lower
    io.ds_to_es_control_signals.reg_wdata_alu := reg_wdata_alu
    io.ds_to_es_control_signals.reg_wdata_pc_off := reg_wdata_pc_off
    io.ds_to_es_control_signals.reg_wdata_mem := reg_wdata_mem

    // branch prediction
    io.branch_new_instr := ds_new_instr & inst_b
    when (ds_new_instr === 1.U) {
        io.branch_br_taken := branch_taken_condition
    } .otherwise {
        io.branch_br_taken := 0.U
    }
}
